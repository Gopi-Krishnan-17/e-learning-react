import React from 'react';
import './Dashboard.css';
import logo from './images/logo.png';
import coding from './images/coding.jpg';
import reactjs from './images/reactjs.jpg';
const Dashboard = () =>{
    return(
        <div className="total">
        <div className="nav">
            <div className="logo"><img src={logo}></img>
            </div>
            <div className="menu">
                <ul>
                    <li><a href="home.html">All Courses</a></li>
                    <li><a href="#">My Dashboard</a></li>
                </ul>
            </div>
            <div className="logout">
                <button className="logbutton"><a href="index.html">Logout</a></button>
            </div>
        </div>
        <div className="content">
            <h1>Greatings Shyam,</h1>
            <h2>Your Dashboard has list of all your enrolled courses</h2>
        </div>
        <div className="card1">
            <div className="image"><img src={reactjs}></img></div>
            <div className="title">
                 <h2>React js Basics</h2> <br />
                 <h4>Course ID : CDL001</h4>
            </div>
            <div className="time1">
                <p> Start Date:<br />19-12-2020</p>
            </div>
            <div className="time2">
                <p>End Date:<br />15-06-2021 </p>
            </div>
        </div>
        <div className="card2">
            <div className="image"><img src={coding}></img></div>
            <div className="title">
                 <h2>Node js Basics</h2> <br />
                 <h4>Course ID : CDL002</h4>
            </div>
            <div className="time1">
                <p> Start Date:<br />19-12-2020</p>
            </div>
            <div className="time2">
                <p>End Date:<br />15-06-2021 </p>
            </div>
        </div>
        </div>
    );
}
export default Dashboard;