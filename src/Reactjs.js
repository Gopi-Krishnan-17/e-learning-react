import  React from 'react';
import './Reactjs.css';
import logo from './images/logo.png';
import reactjs from './images/reactjs.jpg';
const Reactjs = () =>{
    return(
        <div className="total">
        <div className="nav">
            <div className="logo"><img src={logo}></img>
            </div>
            <div className="menu">
                <ul>
                    <div className='menu1'><li><a href="home.html">All Courses</a></li></div>
                    <li><a href="#">My Dashboard</a></li>
                </ul>
            </div>
            <div className="logout">
                <button className="logbutton"><a href="index.html">Logout</a></button>
            </div>
        </div>
        <div className="heading">
            <h1>React js Basics</h1>
        </div>
        <div className='instruct'>
            <p><b />Instructors : <b /> John Doe,Steve Rogers</p>
            <p>Duration : 19 hours</p>
            </div>
        <div className='date'>
            <p>Course Starts : 19-12-2020</p>
            <p>Course Ends : 15-06-2021</p>
        </div>
        <div className='paragraph'>
            <p>React makes it painless to create interactive UIs.Design simple views for each</p>
            <p>state in your application, and React will efficiently update and render just the</p>
            <p>right components when your data changes.</p><br />
            <p>Declarative  views make your code more predictable and easier to debug. Build</p>
            <p>encapsulated components that manage their own state,then compose them to</p>
            <p>make complex UIs.</p><br />
            <p>Since component logic is written in Javascript instead of templates, you can</p>
            <p>easily pass rich data through your app and keep state out of the DOM.</p>
        </div>
        <div className='button'><button>Enroll Now</button></div>
        <div className='image'><img src={reactjs}></img></div>
        </div>
    );
}
export default Reactjs;